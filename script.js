// TODO: Check Tampermonkey

var normalFont = '"Segoe UI",Roboto,"Noto Sans",Ubuntu,Cantarell,"Helvetica Neue",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji"'
var monospaceFont = '"DejaVu Sans Mono","Liberation Mono","Consolas","Ubuntu Mono","Courier New","andale mono","lucida console",monospace';

function setNormalFont(selector)
{
	document.querySelectorAll(selector).forEach((elem) => {
		if (elem.style.fontFamily != normalFont)
			elem.style.fontFamily = normalFont;
	});
}

function setMonospaceFont(selector)
{
	document.querySelectorAll(selector).forEach((elem) => {
		if (elem.style.fontFamily != monospaceFont)
			elem.style.fontFamily = monospaceFont;
	});
}

function fixNormalFont()
{
	let selectors = [
		"body",
		"#search",
		".diff-file",
		".notes_holder",
		".gl-form-input",
	];

	for (let selector of selectors)
		setNormalFont(selector);

	setTimeout(fixNormalFont, 500);
}

function fixMonospaceFont()
{
	let selectors = [
		"table.code",
		".diff-table.code",
		"textarea.js-gfm-input",
		".job-log",
		".commit-sha",
		".ref-name",
		"pre",
		".md code",
		".discussion-reply-holder textarea",
		".reply-placeholder-text-field textarea",
		".git-clone-holder input",
		".gfm-commit",
		".gfm-commit_range",
		".gl-font-monospace",
	];

	for (let selector of selectors)
		setMonospaceFont(selector);

	setTimeout(fixMonospaceFont, 500);
}

fixNormalFont();
fixMonospaceFont();
